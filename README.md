# My i18n Rust library

## Features
- [x] Fully at compile-time (shouldn't ever crash or experience an issue at runtime, any crash or issue at runtime should be reported as an issue)
- ~[x] Supports arguments (not named, it uses [format!()](https://doc.rust-lang.org/stable/std/macro.format.html) under the hood, see its documentation for all of its caracteristics)~
- [x] Supports named arguments (of the form `$argument_name`)
- [ ] Fallback language choice (for now, it is always `en-US`)

## Example
```rust
///This macro generates a function per translation key inside the specified struct.
#[tiny_i18n::i18n("path_of_the_translations_directory")] //defaults to `i18n` if called without argument
pub(crate) struct I18n;

fn main() {
    // Each translation key generates a function (every non-ASCII letter, number or underscore are removed)
    println!("{}", I18n::hello_world("en-us")); //the first argument is the language, the following are the arguments, if any
}
```

The structure is the following :
```
└── *The name of your i18n folder*
    └── en-US
        └── *You can put as many files as you want (with any/no extension) inside each language folder (or inside any subfolder (as many subfolders as you want)), the name of the file (without the extension) is the id of the translation*
    └── *Other languages*
```

