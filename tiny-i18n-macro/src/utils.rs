use std::collections::{HashMap, HashSet};

use crate::TokenType;

/// Delete all the invalid characters for Rust identifiers (so it can be a macro name)
/// Return an error if the result is empty (so if none of the characters is a valid character for Rust identifiers)
pub fn delete_invalid_rust_identifiers_characters(value: &str) -> Result<String, String> {
    let mut is_first = true;
    let value = value
        .chars()
        .filter_map(|c| {
            if is_first {
                if c.is_ascii_alphabetic() || c == '_' {
                    is_first = false;
                    Some(c.to_ascii_lowercase())
                } else {
                    None
                }
            } else {
                if c.is_ascii_alphanumeric() || c == '_' {
                    Some(c.to_ascii_lowercase())
                } else {
                    None
                }
            }
        })
        .collect::<String>();
    if value.is_empty() {
        return Err(format!(
            "Key `{value}` doesn't contain any valid Rust identifier character"
        ));
    }
    Ok(value)
}

pub fn get_fallback_translation_args(fallback_translation: Vec<TokenType>) -> HashSet<String> {
    let mut arguments = HashSet::new();

    //Get the arguments
    for token in fallback_translation {
        match token {
            TokenType::Argument(arg) => {
                arguments.insert(arg);
            }
            _ => {}
        }
    }

    arguments
}

pub fn check_all_languages_have_the_same_args(
    message_id: &str,
    lang_and_translations_without_fallback_lang: HashMap<String, Vec<TokenType>>,
    arguments: &HashSet<String>,
) {
    for (lang, tokens) in lang_and_translations_without_fallback_lang.iter() {
        let mut lang_arguments = HashSet::new();

        //Check that the other languages do not contain any undefined argument
        for token in tokens {
            match token {
                TokenType::Argument(arg) => {
                    if !arguments.contains(arg) {
                        panic!("Translation {message_id} in lang {lang} contains an argument undeclared in the fallback language : {arg}");
                    }
                    lang_arguments.insert(arg.to_string());
                }
                _ => {}
            }
        }

        //Check that the other languages contain all the defined arguments at least once
        for arg in arguments.iter() {
            if !lang_arguments.contains(arg) {
                panic!("Translation {message_id} in lang {lang} does not contain the declared argument {arg}");
            }
        }
    }
}
