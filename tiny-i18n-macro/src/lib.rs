mod code_generation;
mod utils;

use std::{collections::HashMap, fs::File, io::Read, path::PathBuf};

use code_generation::generate_functions;
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput, LitStr};
use utils::delete_invalid_rust_identifiers_characters;

///This macro generates a function per translation key inside the specified struct.
///Example :
///```
///#[tiny_i18n::i18n("path_of_the_translations_directory")] //defaults to `i18n` if called without argument
///pub(crate) struct I18n;
///
///fn main() {
///    // Each translation key generates a function (every non-ASCII letter, number or underscore are removed)
///    println!("{}", I18n::hello_world("en-us")); //the first argument is the language, the following are the arguments, if any
///}
///```
#[proc_macro_attribute]
pub fn i18n(attr_args: TokenStream, stream: TokenStream) -> TokenStream {
    let locales_dir = syn::parse::<LitStr>(attr_args)
        .map(|value| value.value())
        .unwrap_or("i18n".to_string());

    let base_stream: proc_macro2::TokenStream = stream.clone().into();

    let struct_name = parse_macro_input!(stream as DeriveInput).ident;

    let translations = match read_languages_directory_to_hashmap(&locales_dir) {
        Ok(translations) => translations,
        Err(e) => {
            return quote::quote! {
                compile_error!(#e)
            }
            .into()
        }
    };

    let message_id_macros: Vec<_> = translations.into_iter().map(generate_functions).collect();

    quote::quote! {
        #base_stream

        impl #struct_name {
            #(#message_id_macros)*
        }
    }
    .into()
}

/// Read the languages directory and parse it into an HashMap where the key is the ID of the translation and the value is another HashMap where the key is the language and the value is the translation for that language
fn read_languages_directory_to_hashmap(
    dir: &str,
) -> Result<HashMap<String, HashMap<String, Vec<TokenType>>>, String> {
    let root_dir = match std::fs::read_dir(dir) {
        Ok(dir) => dir,
        Err(e) => return Err(format!("Error reading languages directory : {e}")),
    };
    let mut languages_wrongly_sorted = HashMap::new();

    for language_dir in root_dir {
        let language_dir =
            language_dir.map_err(|e| format!("Error reading language directory : {e}"))?;
        let lang = language_dir.file_name();
        let lang = lang.to_str().ok_or(format!(
            "Non Unicode name of folder {}",
            lang.to_string_lossy()
        ))?;
        let metadata = language_dir
            .metadata()
            .map_err(|e| format!("Error getting lang directory `{lang}` metadata : {e}"))?;
        if !metadata.is_dir() {
            return Err(format!(
                "Unexpected file `{lang}` in the languages directory, exiting."
            ));
        }
        let _ = metadata;
        let language_translations = read_language_directory_to_hashmap(language_dir.path())?;
        languages_wrongly_sorted.insert(lang.to_lowercase(), language_translations);
    }

    let translations = organize_hashmap_properly(languages_wrongly_sorted);

    Ok(translations)
}

/// Read a language directory and parse it into an HashMap where the key is the ID of the translation and the value is the translation
fn read_language_directory_to_hashmap(
    dir: PathBuf,
) -> Result<HashMap<String, Vec<TokenType>>, String> {
    let dir = std::fs::read_dir(dir.clone())
        .map_err(|e| format!("Error reading `{}` language directory : {e}", dir.display()))?;
    let mut translations = HashMap::new();

    for file in dir {
        let file = file
            .map_err(|e| format!("Error reading a translation file in the lang directory : {e}"))?;
        let metadata = file.metadata().map_err(|e| {
            format!(
                "Error reading {} translation file metadata : {e}",
                file.path().display()
            )
        })?;
        if !metadata.is_file() {
            let language_hashmap = read_language_directory_to_hashmap(file.path())?;
            for (file_name, tokens) in language_hashmap.into_iter() {
                if translations.insert(file_name.clone(), tokens).is_some() {
                    return Err(format!("Dupplicate translation key `{file_name}`"));
                }
            }
        }
        let _ = metadata;
        let file_name = file.file_name();
        let file_name = file_name.to_str().ok_or(format!(
            "Non Unicode name of file {}",
            file_name.to_string_lossy()
        ))?;
        let file_name = file_name
            .split_once('.')
            .map(|name| name.0)
            .unwrap_or(file_name);
        let file_path = file.path();
        let mut file = File::open(file.path()).map_err(|e| {
            format!(
                "Error opening {} translation file : {e}",
                file.path().display()
            )
        })?;
        let mut file_content = String::new();
        file.read_to_string(&mut file_content)
            .map_err(|e| format!("Error reading {} translation file {e}", file_path.display()))?;
        drop(file);

        let file_name = delete_invalid_rust_identifiers_characters(file_name)?;

        let tokens = parse_arguments(&file_content)?;

        if translations.insert(file_name.clone(), tokens).is_some() {
            return Err(format!("Duplicate translation key `{file_name}`"));
        }
    }

    Ok(translations)
}

///Map `HashMap<lang, HashMap<translation_id, translation>>` to `HashMap<translation_id, HashMap<lang, translation>>`
///
///*Note : only the translations available for the fallback language (en-US for now) are took into account*
fn organize_hashmap_properly(
    mut languages_wrongly_sorted: HashMap<String, HashMap<String, Vec<TokenType>>>,
) -> HashMap<String, HashMap<String, Vec<TokenType>>> {
    let Some(en_translations) = languages_wrongly_sorted.remove("en-us") else {
        panic!("Missing en-US translations");
    };

    let mut translations = HashMap::new();

    for (translation_id, translation_value) in en_translations {
        let mut languages = HashMap::new();
        languages.insert("en-us".to_string(), translation_value);
        for (lang, lang_translations) in &mut languages_wrongly_sorted {
            if let Some(translation) = lang_translations.remove(&translation_id) {
                languages.insert(lang.to_string(), translation);
            }
        }
        translations.insert(translation_id, languages);
    }

    translations
}

#[derive(Clone)]
enum TokenType {
    String(String),
    Argument(String),
}

fn parse_arguments(translation: &str) -> Result<Vec<TokenType>, String> {
    let mut tokens = Vec::new();

    let mut iter = translation.char_indices().into_iter().peekable();

    let mut last_string_token = String::new();

    loop {
        let Some((i, c)) = iter.next() else {
            break;
        };
        match c {
            '$' => {
                if !last_string_token.is_empty() {
                    tokens.push(TokenType::String(std::mem::take(&mut last_string_token)));
                }

                let mut argument_name = String::new();
                loop {
                    let Some((i, next_token)) = iter.peek() else {
                        break;
                    };
                    let (i, next_token) = (*i, *next_token);
                    if argument_name.is_empty() {
                        if !next_token.is_ascii_alphabetic() && next_token != '_' {
                            return Err(format!("Expected alphabetic ASCII character or underscore, found {next_token} at index {i}"));
                        }
                    } else {
                        if !next_token.is_ascii_alphanumeric() && next_token != '_' {
                            break;
                        }
                    }
                    iter.next();
                    argument_name.push(next_token.to_ascii_lowercase());
                }

                if argument_name.is_empty() {
                    return Err(format!("Expected name of argument at index {i}, got EOF"));
                }
                tokens.push(TokenType::Argument(argument_name));
            }
            '\\' => {
                let Some((_, c_to_escape)) = iter.next() else {
                    return Err(format!(
                        "Expected character to escape at index {i}, got EOF"
                    ));
                };
                if c_to_escape != '$' {
                    last_string_token.push('\\');
                }
                last_string_token.push(c_to_escape);
            }
            c => last_string_token.push(c),
        }
    }

    if !last_string_token.is_empty() {
        tokens.push(TokenType::String(last_string_token));
    }

    Ok(tokens)
}
