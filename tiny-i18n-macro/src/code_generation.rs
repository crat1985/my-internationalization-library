use crate::{
    utils::{check_all_languages_have_the_same_args, get_fallback_translation_args},
    TokenType,
};

use std::collections::HashMap;

pub fn generate_functions(
    (message_id, lang_and_translations): (String, HashMap<String, Vec<TokenType>>),
) -> proc_macro2::TokenStream {
    let arguments = generate_arguments(&message_id, &lang_and_translations);

    let mut lang_and_translations: Vec<(String, Vec<TokenType>)> =
        lang_and_translations.into_iter().collect();

    lang_and_translations.sort_by(|a, b| a.0.cmp(&b.0));

    let len = lang_and_translations.len();

    let mut fallback_translation = None;

    let lang_and_translations = lang_and_translations
        .into_iter()
        .map(|value| generate_function_array_element(value, &mut fallback_translation, &message_id))
        .collect::<Vec<_>>();

    let fallback_translation = fallback_translation.expect(&format!(
        "Missing fallback translation for translation {message_id}"
    ));

    let message_id = syn::Ident::new(&message_id, proc_macro2::Span::call_site());

    quote::quote! {
        pub fn #message_id(lang: &str, #(#arguments),*) -> String {
            let lang_translations: [(&str, String); #len] = [
                #(#lang_and_translations),*
            ];

            if let Ok(index) = lang_translations.binary_search_by(|x| x.0.cmp(lang)) {
                lang_translations.get(index).unwrap().1.to_string()
            } else {
                #fallback_translation
            }
        }
    }
}

fn generate_arguments(
    message_id: &str,
    lang_and_translations: &HashMap<String, Vec<TokenType>>,
) -> Vec<proc_macro2::TokenStream> {
    let mut lang_and_translations = lang_and_translations.clone();

    let arguments = get_fallback_translation_args(lang_and_translations.remove("en-us").unwrap());

    check_all_languages_have_the_same_args(message_id, lang_and_translations, &arguments);

    let arguments = arguments
        .into_iter()
        .map(|arg| {
            let arg = syn::Ident::new(&arg, proc_macro2::Span::call_site());

            quote::quote! {#arg: impl std::fmt::Display}
        })
        .collect::<Vec<_>>();

    arguments
}

fn generate_function_array_element(
    (lang, tokens): (String, Vec<TokenType>),
    fallback_translation: &mut Option<proc_macro2::TokenStream>,
    message_id: &str,
) -> proc_macro2::TokenStream {
    let tokens = tokens
        .into_iter()
        .enumerate()
        .map(|(i, token)| {
            let optional_plus = if i == 0 {
                quote::quote! {}
            } else {
                quote::quote! {+ }
            };

            match token {
                TokenType::String(string) => {
                    let optional_to_string = if i == 0 {
                        quote::quote! {.to_string()}
                    } else {
                        quote::quote! {}
                    };

                    quote::quote! {#optional_plus #string #optional_to_string}
                }
                TokenType::Argument(arg) => {
                    let ident = syn::Ident::new(&arg, proc_macro2::Span::call_site());

                    let optional_as_str = if i == 0 {
                        quote::quote! {}
                    } else {
                        quote::quote! {.as_str()}
                    };

                    quote::quote! {#optional_plus #ident.to_string() #optional_as_str}
                }
            }
        })
        .collect::<proc_macro2::TokenStream>();

    if lang == "en-us" {
        if fallback_translation.is_some() {
            panic!("Dupplicate fallback translation for {message_id}");
        }
        *fallback_translation = Some(tokens.clone());
    }

    quote::quote! {
        (#lang, #tokens)
    }
}
